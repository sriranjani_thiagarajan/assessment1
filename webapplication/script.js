function displayMessage() {
    document.getElementById("message").textContent = "Hello, world!";
  }
  
  document.getElementById("myForm").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent default form submission
  
    // Get form values
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
  
    // Display submitted values
    document.getElementById("message").textContent = "Submitted: Name - " + name + ", Email - " + email;
  
    // You can perform further actions here, such as sending the form data to a server or saving it locally.
  });
  